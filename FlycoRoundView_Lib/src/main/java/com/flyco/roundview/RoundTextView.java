package com.flyco.roundview;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.utils.Rect;
import ohos.app.Context;

public class RoundTextView extends Text implements Component.LayoutRefreshedListener {

    private RoundViewDelegate delegate;

    public RoundTextView(Context context) {
        super(context);
    }

    public RoundTextView(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
        delegate = new RoundViewDelegate(this, context, attrSet);
        setLayoutRefreshedListener(this::onRefreshed);
    }

    public RoundTextView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        delegate = new RoundViewDelegate(this, context, attrSet);
        setLayoutRefreshedListener(this::onRefreshed);
    }

    public RoundViewDelegate getDelegate() {
        return delegate;
    }

    @Override
    public void onRefreshed(Component component) {
        int widthSpec = new UiUtil(getContext()).getMeasureSpec(ComponentContainer.LayoutConfig.MATCH_PARENT,
            getWidth());
        int heightSpec = new UiUtil(getContext()).getMeasureSpec(ComponentContainer.LayoutConfig.MATCH_PARENT,
            getHeight());
        onMeasure(widthSpec, heightSpec);
        Rect parentRect = getComponentPosition();
        onLayout(true, parentRect.left, parentRect.top, parentRect.right, parentRect.bottom);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (delegate.isWidthHeightEqual() && getWidth() > 0 && getHeight() > 0) {
            int max = Math.max(getWidth(), getHeight());
            int measureSpec = MeasureSpec.getMeasureSpec(max, MeasureSpec.PRECISE);
            setComponentSize(measureSpec, measureSpec);
            return;
        }

        setComponentSize(widthMeasureSpec, heightMeasureSpec);
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if (delegate.isRadiusHalfHeight()) {
            delegate.setCornerRadius(getHeight() / 2);
        } else {
            delegate.setBgSelector();
        }
    }
}
