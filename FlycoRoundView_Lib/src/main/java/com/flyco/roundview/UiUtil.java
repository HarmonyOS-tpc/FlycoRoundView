/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package com.flyco.roundview;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;
import ohos.interwork.utils.ParcelableEx;
import ohos.utils.Parcel;
import ohos.utils.ParcelException;

public class UiUtil extends ComponentContainer {

    public UiUtil(Context context) {
        super(context);
    }

    int getMeasureSpec(int value, int size) {
        if (value == LayoutConfig.MATCH_PARENT) {
            return MeasureSpec.getMeasureSpec(size, MeasureSpec.PRECISE);
        } else if (value == LayoutConfig.MATCH_CONTENT) {
            return MeasureSpec.getMeasureSpec(size, MeasureSpec.NOT_EXCEED);
        } else {
            return value;
        }
    }

}
