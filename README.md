## FlycoRoundView

## Introduction
A library helps openharmony built-in views easy and convenient to set round rectangle background and accordingly related shape resources can be reduced.

### Demo
<img src="images/FlycoRoundView.PNG" width=450 ></img>

## Usage Instructions
### TextViewDefault
we can see the the background color is changing and text color is not changing while clicking TextView Default
   ```xml
   <com.flyco.roundview.RoundTextView
    ohos:id="$+id:rtv_1"
    ohos:height="match_content"
    ohos:width="match_content"
    ohos:clickable="true"
    ohos:padding="10vp"
    ohos:text="TextView Default"
    ohos:text_color="#383838"
    rv:rv_backgroundColor="#ffffff"
    rv:rv_backgroundPressColor="#383838"
    rv:rv_strokeColor="#383838"
    rv:rv_strokeWidth="1vp"
    rv:rv_textPressColor="#ffffff"/>

```

### RadiusHalf Height
makes the view radius to half and rounded clicking on Radius HalfHeight  Back ground color  is  changing to blue and Text color changing to white
   ```xml
  <com.flyco.roundview.RoundTextView
    ohos:id="$+id:rtv_2"
    ohos:height="match_content"
    ohos:width="match_content"
    ohos:alignment="center"
    ohos:bottom_padding="10vp"
    ohos:clickable="true"
    ohos:left_margin="20vp"
    ohos:left_padding="18vp"
    ohos:long_click_enabled="true"
    ohos:right_padding="18vp"
    ohos:text="Radius Half Height"
    ohos:text_color="#03A9F4"
    ohos:top_padding="10vp"
    rv:rv_backgroundColor="#ffffff"
    rv:rv_backgroundPressColor="#03A9F4"
    rv:rv_isRadiusHalfHeight="true"
    rv:rv_isRippleEnable="false"
    rv:rv_strokeColor="#03A9F4"
    rv:rv_strokePressColor="#6D8FB0"
    rv:rv_strokeWidth="1vp"
    rv:rv_textPressColor="#ffffff"/>

```

### TetView Radius 10vp
On Click the TextView Radius10vp color is changing as per the expectation visible on the UI.
   ```xml
<com.flyco.roundview.RoundTextView
    ohos:id="$+id:rtv_3"
    ohos:height="match_content"
    ohos:width="200vp"
    ohos:alignment="center"
    ohos:bottom_margin="10vp"
    ohos:bottom_padding="10vp"
    ohos:ellipsize="end"
    ohos:left_padding="18vp"
    ohos:right_padding="18vp"
    ohos:singleLine="true"
    ohos:text="TextView Radius 10dpTextView Radius 10vp"
    ohos:text_color="#383838"
    ohos:top_padding="10vp"
    rv:rv_backgroundColor="#F6CE59"
    rv:rv_cornerRadius="10vp"/>
```

### Radius Top Left
The Radius TopLeft displays rounded corner at TopLeft
   ```xml
  <com.flyco.roundview.RoundTextView
    ohos:height="match_content"
    ohos:width="0vp"
    ohos:padding="10vp"
    ohos:right_margin="2.5vp"
    ohos:text="Radius TopLeft"
    ohos:text_alignment="center"
    ohos:text_color="#ffffff"
    ohos:weight="1"
    rv:rv_backgroundColor="#DE88A5"
    rv:rv_cornerRadius_TL="10vp"/>

```
### Radius Top Right
The Radius TopRight displaying with round corner at TopRight
   ```xml
  <com.flyco.roundview.RoundTextView
    ohos:height="match_content"
    ohos:width="0vp"
    ohos:padding="10vp"
    ohos:right_margin="2.5vp"
    ohos:text="Radius TopRight"
    ohos:text_alignment="center"
    ohos:text_color="#ffffff"
    ohos:weight="1"
    rv:rv_backgroundColor="#DE88A5"
    rv:rv_cornerRadius_TR="10vp"/>

```
### Radius Botttom Left
The Radius BottomLeft displaying with round corner at BottomLeft
   ```xml
  <com.flyco.roundview.RoundTextView
    ohos:height="match_content"
    ohos:width="0vp"
    ohos:padding="10vp"
    ohos:right_margin="2.5vp"
    ohos:text="Radius Bottom Left"
    ohos:text_alignment="center"
    ohos:text_color="#ffffff"
    ohos:weight="1"
    rv:rv_backgroundColor="#DE88A5"
    rv:rv_cornerRadius_BL="10vp"/>

```
### Radius Bottom Right
The Radius BottomRight displaying with round corner at BottomRight
   ```xml
  <com.flyco.roundview.RoundTextView
    ohos:height="match_content"
    ohos:width="0vp"
    ohos:padding="10vp"
    ohos:right_margin="2.5vp"
    ohos:text="Radius Bottom Right"
    ohos:text_alignment="center"
    ohos:text_color="#ffffff"
    ohos:weight="1"
    rv:rv_backgroundColor="#DE88A5"
    rv:rv_cornerRadius_BR="10vp"/>

```
### FrameLayout
The FrameLayout as per the expectation visible on the UI with  with image and round corners four sides
   ```xml
 <com.flyco.roundview.RoundFrameLayout
    ohos:height="match_content"
    ohos:width="150vp"
    ohos:bottom_margin="10vp"
    ohos:padding="10vp"
    rv:rv_backgroundColor="#009688"
    rv:rv_cornerRadius="5vp">

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:layout_alignment="center"
        ohos:text="FrameLayout"
        ohos:text_color="#ffffff"/>
</com.flyco.roundview.RoundFrameLayout>

```
### Linear Layout
The Linear Layout displays with Image and background is filled with color as per the expectation visible on the UI.
   ```xml
<com.flyco.roundview.RoundLinearLayout
    ohos:height="match_content"
    ohos:width="match_content"
    ohos:alignment="center"
    ohos:bottom_margin="10vp"
    ohos:left_padding="18vp"
    ohos:orientation="vertical"
    ohos:right_padding="18vp"
    ohos:text_color="#ffffff"
    ohos:top_padding="2vp"
    rv:rv_backgroundColor="#E45171"
    rv:rv_cornerRadius="5vp">

    <Image
        ohos:height="30vp"
        ohos:width="30vp"
        ohos:image_src="$media:ic_portrait"
        ohos:scale_mode="inside"
        ohos:text_color="#ffffff"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="LinearLayout"
        ohos:text_color="#ffffff"/>

</com.flyco.roundview.RoundLinearLayout>

```
### RelativeLayout
The Relative Layout displays with Image and background is filled with color as per the expectation visible on the UI.
   ```xml
<com.flyco.roundview.RoundRelativeLayout
    ohos:height="match_content"
    ohos:width="match_content"
    ohos:bottom_margin="10vp"
    ohos:bottom_padding="10vp"
    ohos:left_padding="18vp"
    ohos:right_padding="18vp"
    ohos:text_color="#ffffff"
    ohos:top_padding="10vp"
    rv:rv_backgroundColor="#655989"
    rv:rv_isRadiusHalfHeight="true">

    <Image
        ohos:id="$+id:iv_portrait"
        ohos:height="30vp"
        ohos:width="30vp"
        ohos:image_src="$media:ic_portrait"
        ohos:scale_mode="inside"
        ohos:text_color="#ffffff"
        ohos:vertical_center="true"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:left_margin="5vp"
        ohos:right_of="$+id:iv_portrait"
        ohos:text="RelativeLayout"
        ohos:text_color="#ffffff"
        ohos:vertical_center="true"/>
</com.flyco.roundview.RoundRelativeLayout>

```

## Installation Instruction

```
Method 1: Generate har package from library and add it to lib folder.
       add following code to gradle of entry
       
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       
Method 2:
    allprojects{
        repositories{
            mavenCentral()
        }
    }

    implementation 'io.openharmony.tpc.thirdlib:FlycoRoundView:1.0.1'
```

## Attributes
|name|format|description|
|:---:|:---:|:---:|
| rv_backgroundColor | color | background color
| rv_backgroundPressColor | color | background press color
| rv_cornerRadius | dimension | background rectangle corner radius,unit vp
| rv_strokeWidth | dimension | background rectangle stroke width,unit vp
| rv_strokeColor | color |background rectangle stroke color
| rv_strokePressColor | color |background rectangle stroke press color
| rv_textPressColor | color |text press color
| rv_isRadiusHalfHeight | boolean | corner radius is half of height
| rv_isWidthHeightEqual | boolean | width and height is the same size which is the max value of them
| rv_cornerRadius_TL | dimension | corner radius top left,unit vp
| rv_cornerRadius_TR | dimension | corner radius top right,unit vp
| rv_cornerRadius_BL | dimension | corner radius bottom left,unit vp
| rv_cornerRadius_BR | dimension | corner radius bottom right,unit vp