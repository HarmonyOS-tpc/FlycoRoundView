/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.flyco.RoundViewSample.slice;

import com.flyco.RoundViewSample.ResourceTable;
import com.flyco.RoundViewSample.utils.ResUtil;
import com.flyco.roundview.RoundTextView;
import com.flyco.roundview.RoundViewDelegate;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;

public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        RoundTextView rtv_1 = (RoundTextView) findComponentById(ResourceTable.Id_rtv_1);
        rtv_1.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new ToastDialog(getContext()).setText("Click--->RoundTextView_1")
                    .setAlignment(LayoutAlignment.BOTTOM)
                    .show();
            }
        });
        RoundTextView rtv_2 = (RoundTextView) findComponentById(ResourceTable.Id_rtv_2);
        rtv_2.setLongClickedListener(new Component.LongClickedListener() {
            @Override
            public void onLongClicked(Component component) {
                new ToastDialog(getContext()).setText("LongClick--->RoundTextView_2")
                    .setAlignment(LayoutAlignment.BOTTOM)
                    .show();
            }
        });
        RoundTextView rtv_3 = (RoundTextView) findComponentById(ResourceTable.Id_rtv_3);
        rtv_3.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                RoundViewDelegate delegate = rtv_3.getDelegate();
                delegate.setBackgroundColor(
                    delegate.getBackgroundColor() == ResUtil.getColor(getContext(), ResourceTable.Color_white)
                        ? ResUtil.getColor(getContext(), ResourceTable.Color_yellow)
                        : ResUtil.getColor(getContext(), ResourceTable.Color_white));
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
