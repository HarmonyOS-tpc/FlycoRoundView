package com.flyco.RoundViewSample;

import com.flyco.roundview.RoundTextView;
import com.flyco.roundview.RoundViewDelegate;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.app.Context;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class RoundViewDelegateTest {

    private AttrSet attrSet = new AttrSet() {
        @Override
        public Optional<String> getStyle() {
            return Optional.empty();
        }

        @Override
        public int getLength() {
            return 0;
        }

        @Override
        public Optional<Attr> getAttr(int i) {
            return Optional.empty();
        }

        @Override
        public Optional<Attr> getAttr(String s) {
            return Optional.empty();
        }
    };

    private Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    RoundTextView roundtextview = new RoundTextView(context, attrSet);

    private RoundViewDelegate  mRoundViewDelegate = new RoundViewDelegate(roundtextview, context, attrSet);

    @Test
    public void testSetBackgroundColor() {
        mRoundViewDelegate.setBackgroundColor(0xffffff);
        assertEquals(0xffffff, mRoundViewDelegate.getBackgroundColor());
    }

    @Test
    public void testSetIsRadiusHalfHeight() {
        mRoundViewDelegate.setIsRadiusHalfHeight(true);
        assertEquals(true, mRoundViewDelegate.isRadiusHalfHeight());
    }
}